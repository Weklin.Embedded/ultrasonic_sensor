# Parameters for compiling

TARGET = output

F_CPU = 16000000UL

SRC = $(wildcard src/*.cpp)
INC = -Iincludes/
OBJ = $(SRC:.cpp=.o)

# Default compiler and programmer
CC = avr-g++
AVRDUDE = avrdude
OBJCOPY=avr-objcopy
# Device name (compiler and programmer)
MCU = atmega32m1
AVRDUDE_MCU = m32m1
AVRDUDE_PROG = avrispmkii
#AVRDUDEFLAGS = -P /dev/ttyACM0 -b 19200
AVRDUDEFLAGS = -P usb
# Default compiler and linker flags
CFLAGS = -c -W -Wall -Werror -mmcu=$(MCU) -Os $(INC) -DF_CPU=$(F_CPU) -std=c++11
LDFLAGS	=


all: $(TARGET).hex

# Create object files
%.o : %.cpp
	$(CC) $(CFLAGS) $^ -o $@

$(TARGET).hex: $(OBJ)
	$(CC) $(LDFLAGS) -mmcu=$(MCU) $^ -o $@

clean:
	rm $(OBJ)
	rm $(TARGET).hex

# Upload hex file in the target
upload:
	$(AVRDUDE) -c $(AVRDUDE_PROG) -p $(AVRDUDE_MCU) $(AVRDUDEFLAGS) -U flash:w:$(TARGET).hex

.PHONY: clean all upload
