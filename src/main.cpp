#include "config.h"

#include <util/delay.h>
#include <avr/io.h>

#include "led.h"
#include "uart.h"
#include "input.h"
#include "hcsr04.h"


int main(void) {
    LED redled(&LED_RED_PORT, LED_RED_PIN, LED_RED_POL);
    UART_init(115200, 1);
    HCSR04 sensor(Output(&US_TRIG_PORT, US_TRIG_PIN), Input(&US_ECHO_PORT, US_ECHO_PIN));

    while(1) {
        redled.blink();

        uint16_t dist = sensor.getDistance();
        printf("Distance = %u\n", dist);

        _delay_ms(250);
    }
}
