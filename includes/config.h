#ifndef CONFIG_H
#define CONFIG_H

#define FOSC    16000
#define F_CPU   16000000UL

#define CAN_BAUDRATE   500        // in kBit

/************************/
/*    UART              */
/************************/

#define			UART_BITRATE            1
#define			UART_BAUDRATE           115200

/************************/
/*    LED               */
/************************/

#define LED_RED_PORT    PORTB
#define LED_RED_PIN     3
#define LED_RED_POL     0

/************************/
/*    ULTRASONIC SENSOR */
/************************/

#define US_TRIG_PORT    PORTC
#define US_TRIG_PIN     0
#define US_ECHO_PORT    PORTD
#define US_ECHO_PIN     6

#endif // CONFIG_H
